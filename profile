<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <link rel="stylesheet" href="me.css">
    <script src="me.js"></script>
  </head>
  <body>
<!--      <div class="vd_title-section clearfix">
        <h1>User Profile Form</h1>
         </div>
        -->
    <div class="wrapper">
      
      <nav id="sidebar">
        
           
        
        <!--<div class="sidebar-header">
          
        </div>-->
        <!--defines for centre image to add/upload 
        Name of the employee
      id of the employee-->
        <center>
           
            <img src="emp-image.jpg">
            <br><br>
           <h3>Employee Name</h3>
           <h5>Employee id</h5>
          
          </center>
        
        <ul class="lisst-unstyled components" style="
          margin-bottom: relative;
          ">
          
         <!--Profile drop down menu with class active showing is default for every page like HOME PAGE-->
          <li class="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" style="padding-top: 1px"><i class="fa fa-user" style="font-size:18px;color: rgb(34, 130, 209)"></i>    Profile</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
              <!--Profile Information -->
              <li>
                <a href="#"><i class="fa fa-id-badge" style="font-size:18px;color:rgb(20, 45, 78)"></i>    Profile Information</a>
              </li>
              <!-- change password section -->
              <li>
                <a href="#"><i class="fa fa-key" style="font-size:18px;color:rgb(20, 45, 78)"></i>    Change Password</a>
              </li>
            </ul>
            
          </li>
          <!--Notification-->
          <li>
            
            <a href="#" ><i class="fa fa-bell" style="font-size:18px;color:rgb(34, 130, 209)"></i>   Notification</a>
          </li>
          <!--Employee-->
          <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-group" style="font-size:18px;color:rgb(34, 130, 209)"></i>    Employee</a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
              <!--Add Employee-->
              <li>
                <a href="#"><i class="fa fa-user-plus" style="font-size:18px;color:rgb(20, 45, 78)"></i>    Add</a>
              </li>
              <!--Remove Employee-->
              <li>
                <a href="#"><i class="fa fa-minus-square" style="font-size:18px;color:rgb(20, 45, 78)"></i>    Remove</a>
              </li>
            </ul>
            <!--Attendance-->
            <li>
              <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-address-book" style="font-size:18px;color:rgb(34, 130, 209)"></i>   Attendance</a>
              <ul class="collapse list-unstyled" id="pageSubmenu1">
                <!--Take attendance-->
                <li>
                  <a href="#"><i class="fa fa-plus" style="font-size:18px;color:rgb(20, 45, 78)"></i>   Take Attendance</a>
                </li>
                <!--View attendance-->
                <li>
                  <a href="#"><i class="fa fa-eye" style="font-size:18px;color:rgb(20, 45, 78)"></i>   View Attendance</a>
                </li>
                
              </ul>
              <ul class="vertical-left">
                <!--Sign Out-->
                <li>
                  <a href="#"><i style="font-size:18px" class="fa">&#xf011;</i>  Sign Out</a>
                </li>
              </ul>
            </li>
            
          </li>
        </ul>
      </nav>
      <div id="content">
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="
          padding-left: 4px;
          padding-right: 4px;
          ">
          <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn  btn-info">
            <i class="fas fa fa-align-left"></i>
            
            <span></span>
            
            </button>
            
          </div>
        </nav>
        <!--main page start-->
        <main class="page-content">
          <div class="container-fluid">
            <h2>Profile-form</h2>
            <hr>
            <!--<div class="row">
              <div class="form-group col-md-12">-->
                <div class="vd_content-section clearfix">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="panel widget light-widget">
                        <div class="panel-heading no-title"> </div>
                <form class="form-horizontal" action="#" role="form">
                  <div class="panel-body">
                    <h2 class="mgbt-xs-20"> Profile: <span class="font-semibold">Mariah Carayban</span> </h2>
                    <br>
                    <div class="row">
                      <div class="col-sm-3 mgbt-xs-20">
                        <div class="form-group">
                          <div class="col-xs-12">
                          
                              
                            <div class="form-img text-center mgbt-xs-15"> <img src="http://www.venmond.com/demo/vendroid/img/avatar/big.jpg"> </div>
                            <div class="form-img-action text-center mgbt-xs-20"> <a class="btn vd_btn  vd_bg-blue" href="javascript:void(0);"><i class="fa fa-cloud-upload append-icon"></i> Upload</a> </div>
                          
                            <br>
                            <div>
                              <table class="table table-striped table-hover">
                                <tbody>
                                  <tr>
                                  </tr>
                                  <tr>
                                    <td>Attendance Rating</td>
                                    <td><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i></td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-9">
                        <h3 class="mgbt-xs-15">Account</h3>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <input type="email" placeholder="email@yourcompany.com">
                              </div>
                              <!-- col-xs-12 -->
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Username</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <input type="text" placeholder="username">
                              </div>
                              <!-- col-xs-9 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        
                        
                        <hr>
                        <h3 class="mgbt-xs-15">Profile Setting</h3>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">First Name</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <input type="text" placeholder="first name">
                              </div>
                              <!-- col-xs-9 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Last Name</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <input type="text" placeholder="last name">
                              </div>
                              <!-- col-xs-9 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Gender</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <span class="vd_radio radio-info">
                                  <input type="radio" checked="" value="option3" id="optionsRadios3" name="optionsRadios2">
                                  <label for="optionsRadios3"> Male </label>
                                </span>
                                <span class="vd_radio  radio-danger"> 
                                  
                                  <input type="radio" value="option4" id="optionsRadios4" name="optionsRadios2">
                                  <label for="optionsRadios4"> Female </label>
                                </span> 
                                  
                                
                              </div>
                              <!-- col-xs-9 -->
                             
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Birthday</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <input type="text" id="datepicker-normal" class="width-40 hasDatepicker">
                              </div>
                              <!-- col-xs-12 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Marital Status</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <select class="width-40">
                                  <option>Single</option>
                                  <option>Married</option>
                                </select>
                              </div>
                              <!-- col-xs-9 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">Position</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <select class="width-40">
                                  <option>CEO</option>
                                  <option>Director</option>
                                  <option>Manager</option>
                                  <option>Staff</option>
                                  <option>Office Boy</option>
                                </select>
                              </div>
                              <!-- col-xs-12 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <div class="form-group">
                          <label class="col-sm-3 control-label">About</label>
                          <div class="col-sm-9 controls">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-9">
                                <textarea rows="3"></textarea>
                              </div>
                              <!-- col-xs-12 -->
                              
                            </div>
                            <!-- row --> 
                          </div>
                          <!-- col-sm-10 --> 
                        </div>
                        <!-- form-group -->
                        
                        <hr>
                        <!-- form-group --> 
                        
                      </div>
                      <!-- col-sm-12 --> 
                    </div>
                    <!-- row --> 
                    
                  </div>
                  
                  <div class="pd-20">
                    <button class="btn vd_btn vd_bg-green col-md-offset-3"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Finish</button>
                  </div>
                </form>
              </div>
            
          </div>
          
        </div>
      
        
      </div>
            <!---  <div class="form-group col-md-12">
                <iframe src="https://ghbtns.com/github-btn.html?user=azouaoui-med&repo=pro-sidebar-template&type=star&count=true&size=small" frameborder="0" scrolling="0" width="90px" height="30px"></iframe>
                <iframe src="https://ghbtns.com/github-btn.html?user=azouaoui-med&repo=pro-sidebar-template&type=fork&count=true&size=small" frameborder="0" scrolling="0" width="90px" height="30px"></iframe>
              </div>-->
              
            </div>
            
            <hr>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                
              </div>
              <!--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                
              </div>-->
            </div>
          <hr>
      
           <!--   <footer class="text-center">
              
              
          </footer>-->
          </div>
        </main>
        
        <!--main page end-->
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
    $(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
    });
    });
    
    </script>
  </body>
</html>